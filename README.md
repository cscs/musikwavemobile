Project is hosted here:
https://gitlab.com/cscs/musikwavedesktop

![MusikWave Screenshot 1|50%](https://i.postimg.cc/28YkwWtH/musikwave-screenshot1.png)

![MusikWave Screenshot 2|50%](https://i.postimg.cc/9FBWNM5c/musikwave-screenshot2.png)

If you want to test it out you can simply do:
```
curl -O https://gitlab.com/cscs/musikwavedesktop/-/archive/master/musikwavedesktop-master.tar.gz
```
```
tar xvf musikwavedesktop-master.tar.gz --transform s/musikwavedesktop-master/musikwave-portable/ ; rm musikwavedesktop-master.tar.gz
```
```
cd musikwave
```
```
./musikwave
```

Thats it. 

Everything the app needs to run is in the directory. You can move it anywhere you want, such as your desktop or a thumbdrive.

If you want to install musikwave onto your system you can use the Makefile:

```
sudo make install
```

Or to uninstall:

```
sudo make uninstall
```

Built with electron and nativefier. The unpackaged archive weighs ~130.50 MB

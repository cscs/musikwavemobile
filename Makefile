# GNU make is required to run this file. To install on *BSD, run:
#   gmake PREFIX=/usr/local install

PREFIX ?= /usr
CURRENT_DIR:=$(shell dirname $(realpath $(lastword $(MAKEFILE_LIST))))
PARENT_DIR=$(shell dirname "$(CURRENT_DIR)")
BAD_NAME:=$(basename "$(CURRENT_DIR)")
GOOD_NAME:=musikwave

all:

install:  
	mv "$(CURRENT_DIR)" "$(PARENT_DIR)/$(GOOD_NAME)"
	mkdir -p $(DESTDIR)$(PREFIX)/share
	cp -R $(PARENT_DIR)/$(GOOD_NAME) $(DESTDIR)$(PREFIX)/share
	mkdir -p $(DESTDIR)$(PREFIX)/share/applications
	cp $(PARENT_DIR)/$(GOOD_NAME)/musikwave.desktop $(DESTDIR)$(PREFIX)/share/applications
	mkdir -p $(DESTDIR)$(PREFIX)/share/pixmaps
	cp $(PARENT_DIR)/$(GOOD_NAME)/resources/app/icon.png $(DESTDIR)$(PREFIX)/share/pixmaps/musikwave.png
	mkdir -p $(DESTDIR)$(PREFIX)/bin
	printf "#!/bin/sh\nexec /usr/share/musikwave/musikwave" > $(DESTDIR)$(PREFIX)/bin/musikwave
	chmod +x $(DESTDIR)$(PREFIX)/bin/musikwave
	cd .

uninstall:
	-rm -rf $(DESTDIR)$(PREFIX)/bin/musikwave
	-rm -rf $(DESTDIR)$(PREFIX)/share/musikwave
	-rm -rf $(DESTDIR)$(PREFIX)/share/pixmaps/musikwave.png
	-rm -rf $(DESTDIR)$(PREFIX)/share/applications/musikwave.desktop

clear-cache:
	-rm -rf $(HOME)/.config/musikwave
#
#_get_version:
#	$(eval VERSION := $(shell git show -s --format=%cd --date=format:%Y%m%d HEAD))
#	@echo $(VERSION)
#

.PHONY: all install uninstall #_get_version

# .BEGIN is ignored by GNU make so we can use it as a guard
.BEGIN:
	@head -3 Makefile
	@false
